% Created 2021-01-24 dim. 19:33
% Intended LaTeX compiler: pdflatex
\documentclass[pagesize,paper=a4,11pt,DIV=20]{scrartcl}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{grffile}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{textcomp}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\usepackage{minted}\usemintedstyle{emacs}\usepackage{algorithm2e}
\usepackage{tikz}\usepackage{pgfplots}
\DeclareMathOperator{\Tr}{Tr}
\author{Erwan Giry, Mathieu Fauvel \& Clément Mallet}
\date{\today}
\title{Model Semi-Supervised with Clustering and ADMM\\\medskip
\large ANR MAESTRIA}
\hypersetup{
 pdfauthor={Erwan Giry, Mathieu Fauvel \& Clément Mallet},
 pdftitle={Model Semi-Supervised with Clustering and ADMM},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 26.3 (Org mode 9.1.9)}, 
 pdflang={English}}
\begin{document}

\maketitle
\tableofcontents

\section{Robust Mixture Discriminant Analysis}
\label{sec:org8ad0228}
\subsection{Mixture model}
\label{sec:orgca71c69}
The mixture model is given by \cite{bouveyron-2009-robus-super}.  We  have  two structures in the data set:
\begin{enumerate}
\item An unsupervised structure of \(K\) clusters, associated to the random variables \(S\),
\item A supervised structure of \(C\) classes, associated to the random variable \(Z\).
\end{enumerate}
and we observe \(\big\{\mathbf{x}_\ell,   z_\ell\big\}_{l=1}^n\)  training   samples,  where \(\mathbf{x}_\ell\in\mathbb{R}^d\) are independent realisations of the random vector \(X\) and \(z_\ell\) are the associated labels, independent realisations of \(Z\). The conventional mixture model assumes that
\begin{eqnarray}
  p(\mathbf{x}) = \sum_{j=1}^Kp(\mathbf{X}=\mathbf{x}|S=j)p(S=j).\label{eq:mm}
\end{eqnarray}

If we assume that there is a structure classes represent all the data set, we have the following equality:
\begin{eqnarray}
  \sum_{i=1}^Cp(Z=i|S=j) = 1,\ \forall j\in\{1,\ldots,K\}\label{eq:ck}
\end{eqnarray}
Plugging \ref{eq:ck} into \ref{eq:mm} we obtain
\begin{eqnarray}
  \label{eq:mmm}
  p(\mathbf{x}) = \sum_{i=1}^C\sum_{j=1}^Kp(Z=i|S=j)p(\mathbf{X}=\mathbf{x}|S=j)p(S=j).
\end{eqnarray}
The term \(p(Z=i|S=j)\) can be interpreted as the probability that the \(j\)th clusters belongs to the \(c\)th class. For simplicity, it is denoted \(r_{ij}\) in the following.

For the mixture model, \cite{bouveyron-2009-robus-super} propose to use the HDDA model \cite{bouveyron-2007-high-dimen}. But any clustering model can be used.
\subsection{Classification step}
\label{sec:org349b0e0}
Using the \emph{maximum a posteriori} rule, we have to compute
\begin{eqnarray}\label{eq:map:1}
  p(Z=i|\mathbf{X}=\mathbf{x}) = \frac{p(\mathbf{X}=\mathbf{x}|Z=i)p(Z=i)}{p(\mathbf{X}=\mathbf{x})}
\end{eqnarray}
From \ref{eq:mmm}, we can write the left upper part of \ref{eq:map:1} as \(\sum_{j=1}^Kr_{ij}p(\mathbf{X}=\mathbf{x}|S=j)p(S=j)\) and noting that
$$\frac{p(\mathbf{X}=\mathbf{x}|S=j)p(S=j)}{p(\mathbf{x})}=p(S=j|\mathbf{X}=\mathbf{x})$$
the MAP rule is
\begin{eqnarray}
  \label{eq:map}
  p(Z=i|\mathbf{X}=\mathbf{x}) = \sum_{j=1}^Kr_{ij}p(S=j|\mathbf{X}=\mathbf{x}).
\end{eqnarray}
\subsection{Estimation}
\label{sec:orgffb5a66}
Once the clustering mixture model has been found, the term \(r_{ij},\ \forall (i,j)\in\{1,\ldots,C\}\times\{1,\ldots,K\}\) can be found by maximizing the log-likelihood \cite{bouveyron-2009-robus-super}. Let us note:
\begin{itemize}
\item \(\mathbf{r}_i=\big[r_{i1},\ldots,r_{iK}\big]^\top\ \forall i\in\{1,\ldots,C\}\),
\item \(\mathbf{R}=\big[\mathbf{r}_1,\ldots,\mathbf{r}_C\big]^\top\), \(\mathbf{R}\in\mathbb{R}^{C\times K}\),
\item \(\boldsymbol{\psi}_\ell = \big[p(S=1|\mathbf{X}=\mathbf{x}),\ldots, p(S=K|\mathbf{X}=\mathbf{x})\big]^\top\), \(\forall \ell \in \{1,\ldots,n\}\)
\end{itemize}

Under the positivity and sum-to-one condition and i.i.d assumption for the samples, the likelihood can be written as 
\begin{eqnarray*}
  L(\mathbf{X}, \mathbf{z}, \mathbf{r}) &=& \prod_{\ell=1}^n p(\mathbf{X}=\mathbf{x}_\ell, Z=z_\ell) \\
  &=& \prod_{\ell=1}^np(Z=z_\ell |\mathbf{X}=\mathbf{x}_\ell)p(\mathbf{X}=\mathbf{x}_\ell)
\end{eqnarray*}
The negative log-likelihood is equal to 
\begin{eqnarray*}
  l(\mathbf{X}, \mathbf{z}, \mathbf{r}) & = & \sum_{i=1}^C\sum_{\mathbf{x}_\ell | z_\ell=i}-\ln\big\{p(Z=z_\ell |\mathbf{X}=\mathbf{x}_\ell)\big\} +\xi \\
                                                 & = &\sum_{i=1}^C\sum_{\mathbf{x}_\ell | z_\ell=i} -\ln\Big\{\sum_{j=1}^Kr_{ij}p(S=j|\mathbf{X}=\mathbf{x})\Big\} +\xi \\
                                                 & = &\sum_{i=1}^C\sum_{\mathbf{x}_\ell | z_\ell=i} -\ln\Big\{\mathbf{r}_i^\top\boldsymbol{\psi}_\ell\Big\} +\xi \\
                                                 & = &\sum_{\ell=1}^n -\ln\Big\{\boldsymbol{\beta}_\ell^\top\mathbf{R}\boldsymbol{\psi}_\ell\Big\} +\xi 
\end{eqnarray*}
where \(\xi=-\sum_{\ell=1}^{n}\ln\big\{p(\mathbf{x}_\ell)\big\}\) does not depend on \(\mathbf{R}\) and \(\boldsymbol{\beta}_\ell\in\mathbb{R}^C\) with \(\beta_{\ell i}=1\) if \(i=z_\ell\) otherwise \(\beta_{\ell i}=0\). 

The optimization can be stated as

\begin{eqnarray}
  \label{eq:ll}
  \begin{array}{rl}
    \displaystyle{\min_{\mathbf{R}}}
   &\displaystyle{\sum_{\ell=1}^n -\ln\big(\boldsymbol{\beta}_\ell^\top\mathbf{R}\boldsymbol{\psi}_\ell\big)}\\
   \text{Constraint to} & \mathbf{R}^\top\mathbf{1}_C = \mathbf{1}_K\\
  & \mathbf{R} \succcurlyeq 0,
  \end{array}
  \end{eqnarray}
or equivalently

\begin{eqnarray}
  \label{eq:ll:c}
  \begin{array}{rl}
\displaystyle{\min_{\mathbf{R}}}
&\displaystyle{\sum_{i=1}^C\sum_{\mathbf{x}_\ell | z_\ell=i}  -\ln\big(\mathbf{r}_i^\top \boldsymbol{\psi}_\ell\big)}
 \\
   \text{Constraint to} & \mathbf{R}^\top\mathbf{1}_C = \mathbf{1}_K\\
  & \mathbf{R} \succcurlyeq 0.
  \end{array}
  \end{eqnarray} 

\section{Generic solver}
\label{sec:org5f593af}
In their original formulation, \cite{bouveyron-2009-robus-super} have used a generic solver to optimize  (\ref{eq:ll:c}).

The gradient of eq. (\ref{eq:ll:c}) w.r.t \(\mathbf{r}_i\) is

\begin{equation}
-\sum_{\mathbf{x}_\ell | z_\ell=i} \frac{\boldsymbol{\psi}_\ell}{\mathbf{r}_i^\top \boldsymbol{\psi}_\ell}.
\end{equation}

Equivalently, the gradient of eq. \ref{eq:ll} w.r.t \(\mathbf{R}\) is 
\begin{equation}
-\sum_{\ell=1}^n \frac{\boldsymbol{\beta}_\ell\boldsymbol{\psi}_\ell^\top}{\boldsymbol{\beta}_\ell^\top\mathbf{R}\boldsymbol{\psi}_\ell}.
\end{equation}

In order to reproduce the results of \cite{bouveyron-2009-robus-super} , we use the SQLSP Solver, from the Python SciPy Library. This solver uses Sequential Least SQuares Programming, allowing the minimization of a function, while respecting some equality and inequality constraints.

\section{Solving the minimization problem}
\label{sec:org37bff86}
Recent similar works, such as \cite{bioucas-dias-2014-alter-markov} and \cite{liu-2018-convex-formul} have shown it is possible to write (\ref{eq:ll}) as a convex optimization problem, which has appealing properties, especially for large scale learning.

\subsection{Is \ref{eq:ll} a convex optimization problem ?}
\label{sec:orgc9a7141}
Let us note
\begin{itemize}
\item \(f(\mathbf{R})=\sum_{\ell=1}^{n} -\ln\big(\boldsymbol{\beta}_\ell^\top\mathbf{R}\boldsymbol{\psi}_\ell\big)_{+}\) where \((\cdot)_{+}\) is \(\max(0,\cdot)\).
\item \(g_{1}(\mathbf{R}) = 0\) if \(r_{ij}\geq 0, \forall (i,j)\in\{1,\ldots,C\}\times\{1,\ldots,K\}\) and \(+\infty\) otherwise
\item \(g_{2}(\mathbf{R})=0\) if \(\mathbf{R}^\top\mathbf{1}_C = \mathbf{1}_{K}\) and \(+\infty\) otherwise
\end{itemize}

\textbf{Proposition}: All the function \(f, g_1\) and \(g_2\) are convex.

\begin{itemize}
\item In order to show that \(f\) is convex, we use composition rules \cite{boyd-2004-conve-optim}:
\begin{itemize}
\item \(f\) is the summation of \(n\) functions \(h_\ell(\mathbf{R}) = - \ln\big(\boldsymbol{\beta}_\ell^\top\mathbf{R}\boldsymbol{\psi}_\ell\big)_{\text{+}}\)
\item \(h_\ell(\mathbf{R})\) is the composition of a three convex functions,
\begin{itemize}
\item Linear operator,
\item Max scalar function,
\item Negative logarithm scalar function,
\end{itemize}
therefore it is convex.
\item Hence \(f\) is convex
\end{itemize}

\item \(g_{1}\) is the indicator function on the set \(\mathbb{R}_+\) and it is convex, see \cite{boyd-2004-conve-optim}.
\item Same comments applied to \(g_2\).
\end{itemize}

Hence \ref{eq:ll} is a convex constraint optimization problem and dedicated solver can be used.

\subsection{Consensus approach - ADMM Solver}
\label{sec:org9a18735}
ADMM provides all the material to solve problem such as  (\ref{eq:ll}), see \cite{boyd-2010-distr-optim} for a comprehensive review. We can formulate \ref{eq:ll} as a consensus problem, where we defined one \(\mathbf{R}\) per sample and impose a shared variable with simplex constraints as a global consensus \cite{boyd-2010-distr-optim}.

The optimization problem can be written as:
\begin{eqnarray}
  \label{eq:consensus}
   \begin{array}{rl}
     \displaystyle{\min_{\mathbf{R}_\ell,\mathbf{Z}}} & \displaystyle{\sum_{\ell = 1}^{n}f_\ell(\mathbf{R}_\ell)} + g(\mathbf{Z})\\
     \text{Constraint to} & \mathbf{R}_\ell - \mathbf{Z} = 0\ \forall \ell\in\{1,\ldots,\ n\}
   \end{array}
\end{eqnarray}
where \(f_\ell(\mathbf{R}_\ell)  = -\ln(\boldsymbol{\beta}_\ell^\top\mathbf{R}_\ell\boldsymbol{\psi}_\ell)_{+}\) for \(\ell\in \{1,\ldots,n\}\), and \(g(\mathbf{Z})\) is defined as

\begin{eqnarray}
  g(\mathbf{Z}) & = & \left\{
                      \begin{array}{rl}
                        0 & \text{ if } \mathbf{Z}\in\mathcal{C} = \{\mathbf{Z}|\mathbf{Z}\succcurlyeq 0, \mathbf{Z}^\top\mathbf{1}_C = \mathbf{1}_{K}\} \\
                        +\infty & \text{ Otherwise.} 
                      \end{array}
                                  \right.                                  \label{eq:consensus:simplex}
\end{eqnarray}

The augmented Lagrangian is 
\begin{eqnarray}
  \mathcal{L}_{\rho}(\mathbf{R}_\ell,\ \mathbf{Z},\mathbf{Y}) = \sum_{\ell=1}^{n} \Big(f_{\ell}(\mathbf{R}_\ell) + \langle\mathbf{Y}_\ell, \mathbf{R}_\ell - \mathbf{Z}\rangle_F + \frac{\rho}{2}\|\mathbf{R}_\ell - \mathbf{Z}\|^2_F\Big) + g(\mathbf{Z}).
  \label{eq:consensus:lagrangian}  
\end{eqnarray}
The resulting ADMM algorithm is the following:
\begin{flalign}
  \mathbf{R}_{\ell}^{(t+1)} &=\arg\min_{\mathbf{R}_\ell}\Big\{f_{\ell}(\mathbf{R}_\ell) + \langle\mathbf{Y}_\ell^{(t)}, \mathbf{R}_\ell - \mathbf{Z}^{(t)}\rangle_F + \frac{\rho^{(t)}}{2}\|\mathbf{R}_\ell - \mathbf{Z}^{(t)}\|^2_F\Big\},\label{eq:consensus:Rl}\\
  \mathbf{Z}^{(t+1)} & = \arg\min_{\mathbf{Z}}\Big\{g(\mathbf{Z}) + \sum_{\ell=1}^{n} \Big(-\langle\mathbf{Y}_\ell^{(t)}, \mathbf{Z}\rangle_F + \frac{\rho^{(t)}}{2}\|\mathbf{R}_\ell^{(t+1)} - \mathbf{Z}\|^2_F\Big)\Big\},\label{eq:consensus:Z}\\
  \mathbf{Y}^{(t+1)}_\ell & = \mathbf{Y}^{(t)}_\ell + \rho^{(t)}\big(\mathbf{R}_\ell^{(t+1)}-\mathbf{Z}^{(t+1)}\big).\label{eq:consensus:Y}
\end{flalign}

\subsubsection{\(\mathbf{R}_{\ell}\)-update}
\label{sec:orged0229b}
The core idea of this consensus approach is based on the possibility to have a fast/explicit solution to each sub-problem in eq. (\ref{eq:consensus:Rl}). In the following, we show that such solution exists in our setting. Let \(F_\ell\) be the function to be minimize at iteration \(t\):
\begin{eqnarray}\label{eq:consensus:fun:ell}
  F_\ell(\mathbf{R}_\ell) & = & -\ln(\boldsymbol{\beta}_\ell^\top\mathbf{R}_\ell\boldsymbol{\psi}_\ell)_{+} + \langle\mathbf{Y}_\ell^{(t)}, \mathbf{R}_\ell - \mathbf{Z}^{(t)}\rangle_F + \frac{\rho^{(t)}}{2}\|\mathbf{R}_\ell - \mathbf{Z}^{(t)}\|^2_F.
\end{eqnarray}
Its derivatives w.r.t. \(\mathbf{R}_\ell\) is
\begin{eqnarray}\label{eq:consensus:grad}
  \nabla_{\mathbf{R}_\ell} F_\ell(\mathbf{R}_\ell)& = & -\frac{\boldsymbol{\beta}_\ell\boldsymbol{\psi}_\ell^\top}{\boldsymbol{\beta}_\ell^\top\mathbf{R}_\ell\boldsymbol{\psi}_\ell} + \mathbf{Y}_\ell^{(t)} + \rho^{(t)}\big(\mathbf{R}_\ell - \mathbf{Z}^{(t)}\big).
\end{eqnarray}
Let us note that \(\boldsymbol{\beta}^\top\mathbf{R}_\ell\boldsymbol{\psi}_\ell = \langle\boldsymbol{\beta}_\ell\boldsymbol{\psi}_\ell^\top, \mathbf{R}_\ell\rangle_F\). We are looking for \(\hat{\mathbf{R}}_\ell\) such that the gradient vanishes: such solution is also solution of \(\langle \boldsymbol{\beta}_\ell\boldsymbol{\psi}_\ell^\top,\nabla_{\mathbf{R}_\ell} F_\ell(\hat{\mathbf{R}}_\ell)\rangle_F=0\). Using eq. (\ref{eq:consensus:grad}) in the previous Frobenius product and arranging terms leads to:
\begin{eqnarray}\label{eq:consensus:sol:pol}
-\|\boldsymbol{\beta}_\ell\boldsymbol{\psi}_\ell^\top\|_F^2 +\big\langle\boldsymbol{\beta}_\ell\boldsymbol{\psi}_\ell^\top,\hat{\mathbf{R}}_\ell\big\rangle_F\big\langle\boldsymbol{\beta}_\ell\boldsymbol{\psi}_\ell^\top,\mathbf{Y}_\ell^{(t)}-\rho^{(t)}\mathbf{Z}^{(t)}\big\rangle_F + \rho^{(t)}\big\langle\boldsymbol{\beta}_\ell\boldsymbol{\psi}_\ell^\top,\hat{\mathbf{R}}_\ell\big\rangle_F^2&=&0  
\end{eqnarray}
We have second order polynomial expression w.r.t \(\big\langle\boldsymbol{\beta}_\ell\boldsymbol{\psi}_\ell^\top, \hat{\mathbf{R}}_\ell\big\rangle_F\) whose positive root is (a negative root cannot be solution of eq. (\ref{eq:consensus:fun:ell}))
\begin{eqnarray}
  \label{eq:consensus:nu}
  \big\langle\boldsymbol{\beta}_\ell\boldsymbol{\psi}_\ell^\top, \hat{\mathbf{R}}_\ell\big\rangle_F & = & \frac{1}{2}\big\langle\boldsymbol{\beta}_\ell\boldsymbol{\psi}_\ell^\top,\mathbf{Z}^{(t)}-\frac{\mathbf{Y}^{(t)}_\ell}{\rho^{(t)}}\big\rangle_F + \sqrt{\frac{1}{4}\big\langle\boldsymbol{\beta}_\ell\boldsymbol{\psi}_\ell^\top, \mathbf{Z}^{(t)}-\frac{\mathbf{Y}^{(t)}_\ell}{\rho^{(t)}}\rangle_F^2 + \frac{\|\boldsymbol{\beta}_\ell\boldsymbol{\psi}_\ell^\top\|^2_F}{\rho^{(t)}}}\nonumber\\
& :=& \nu_\ell^{(t)}
\end{eqnarray}
By plugging eq. (\ref{eq:consensus:nu}) in eq. (\ref{eq:consensus:grad}) we  get the optimal solution for eq. (\ref{eq:consensus:fun:ell}):
\begin{eqnarray}\label{eq:consensus:update:Rl}
\mathbf{R}^{(t+1)}_\ell &=& \mathbf{Z}^{(t)} + \frac{1}{\rho^{(t)}}\Big(\frac{\boldsymbol{\beta}_\ell\boldsymbol{\psi}_\ell^\top}{\nu_\ell^{(t)}}-\mathbf{Y}^{(t)}_\ell\Big),~\forall \ell\in\{1,\ldots,n\}.\end{eqnarray}

\subsubsection{\(\mathbf{Z}\)-update}
\label{sec:org928a696}
By collecting linear and quadratic terms w.r.t \(\mathbf{Z}\) and dropping irrelevant terms, the eq. (\ref{eq:consensus:Z})  becomes
\begin{flalign}
  \mathbf{Z}^{(t+1)} & = \arg\min_{\mathbf{Z}}\Big\{g(\mathbf{Z}) + \frac{n\rho^{(t)}}{2}\|\mathbf{Z} - \big(\overline{\mathbf{R}}^{(t+1)}+\frac{\overline{\mathbf{Y}}^{(t)}}{\rho^{(t)}}\big)\|^2_F\Big\}.\label{eq:consensus:Z:b}
\end{flalign}
A solution of eq. (\ref{eq:consensus:Z:b}) can be found iteratively, at a relatively low cost. First, the Lagrangian of the above problem can be written as
\begin{eqnarray}
  \mathcal{L}(\mathbf{Z}, \boldsymbol{\lambda}) & = \pi_{\mathbb{R}^n_+}(\mathbf{Z}) + \frac{n\rho^{(t)}}{2}\|\mathbf{Z} - \big(\overline{\mathbf{R}}^{(t+1)}+\frac{\overline{\mathbf{Y}}^{(t)}}{\rho^{(t)}}\big)\|^2_F + \boldsymbol{\lambda}^\top\big(\mathbf{Z}^\top\mathbf{1}_C - \mathbf{1}_{K}\big),
\end{eqnarray}
where \(\pi_{\mathbb{R}^n_+}\) is the indicator function of \(\mathbb{R}^n_+\) and \(\boldsymbol{\lambda}\in\mathbb{R}^K\). By taking the derivative of the Lagrangian w.r.t \(\mathbf{Z}\) we obtain the following solution
\begin{eqnarray}
  \label{eq:consensus:Z:lagrangian:dual}
  \mathbf{Z} = \Big(\overline{\mathbf{R}}^{(t+1)}+\frac{\overline{\mathbf{Y}}^{(t)}}{\rho^{(t)}} - \frac{\mathbf{1}_C\boldsymbol{\lambda}^\top}{n\rho^{(t)}}\Big)_{+}.
\end{eqnarray}
We need to find \(\boldsymbol{\lambda}\) such as the primal feasibility is reached, i.e., the constraints is satisfied. Because the constraints are linear, they are decoupled w.r.t the column of \(\mathbf{Z}\). Therefore, we have for all \(q\in\{1,\ldots,K\}\)
\begin{eqnarray*}
  \sum_{p=1}^C\mathbf{Z}_{pq} &=& 1\\
  \sum_{p=1}^C\Big(\overline{\mathbf{R}}_{pq}^{(t+1)}+\frac{\overline{\mathbf{Y}}_{pq}^{(t)}}{\rho^{(t)}} - \frac{\boldsymbol{\lambda}_q}{n\rho^{(t)}}\Big)_{+} & = &  1 \\
  \sum_{p=1}^C\Big(n\rho^{(t)}\overline{\mathbf{R}}_{pq}^{(t+1)}+n\overline{\mathbf{Y}}_{pq}^{(t)} - \boldsymbol{\lambda}_q\Big)_{+} & = & n\rho^{(t)} \\
  H(\boldsymbol{\lambda}_{q}) & = & 1.\label{eq:consensus:lagrangian:primal}
\end{eqnarray*}
Solution of eq. (\ref{eq:consensus:lagrangian:primal}) can be found by bisection, as shown in Figure \ref{fig:H}. Once the value 1 has been framed between two adjacent values, we have the analytic expression of optimal \(\boldsymbol{\lambda}_p\).

\begin{figure}
  \centering
  \begin{tikzpicture}
    \begin{axis}[grid,small,xmin=0,xmax=10,ymin=0,ymax=4.2]
      \addplot [domain=0:10, samples=100, thick,blue] {max(0,1-x)/4 + max(0,2.5-x)/4 + max(0,7.5-x)/4 + max(0,9.0-x)/4};
      %\addplot [domain=0:10, samples=100,] {1};
      \addplot [only marks, gray] coordinates {(1, 4) (2.5, 2.875) (7.5, 0.375) (9, 0)};
      \addplot [dashed] coordinates {(1, 0) (1, 4) (0,4)};
      \addplot [dashed] coordinates {(2.5, 0) (2.5, 2.875) (0, 2.875)};
      \addplot [dashed] coordinates {(7.5, 0) (7.5, 0.375) (0, 0.375)};
      \addplot [only marks, red] coordinates {(6.25, 1)};
    \end{axis}
  \end{tikzpicture}
  \caption{Example of \(H\) function: max(0,1-x)/4 + max(0,2.5-x)/4 + max(0,7.5-x)/4 + max(0,9.0-x)/4. The function is picewise-linear with known breakpoints.}
  \label{fig:H}
\end{figure}

\subsubsection{Optimality conditions and stopping criterion}
\label{sec:orgbd7415e}
Primal and dual residuals are, respectively:
\begin{eqnarray*}
  \mathbf{Pr}^{(t)} = \big[\mathbf{R}_1^{(t)} - \mathbf{Z}^{(t)}, \ldots,\mathbf{R}_{n}^{(t)} - \mathbf{Z}^{(t)}\big],
\end{eqnarray*}
and
\begin{eqnarray*}
  \mathbf{Dr}^{(t)} = \rho^{(t)} \big[\mathbf{Z}^{(t)} - \mathbf{Z}^{(t-1)}, \ldots,\mathbf{Z}^{(t)} - \mathbf{Z}^{(t-1)}\big].
\end{eqnarray*}
Their squared norms are
\begin{eqnarray}
  \|\mathbf{Pr}^{(t)}\|^2_F & = & \sum_{\ell=1}^{n}\|\mathbf{R}_\ell^{(t)} - \mathbf{Z}^{(t)}\|^2_F, \\
  \|\mathbf{Dr}^{(t)}\|^2_F & = &  n\rho^{(t)}\|\mathbf{Z}^{(t)} - \mathbf{Z}^{(t-1)}\|^2_F.
\end{eqnarray}
A termination criterion is to check that both \(\mathbf{Pr}\) and \(\mathbf{Dr}\)  be small, \emph{i.e.}, 
\begin{eqnarray}
  \label{eq:stop:criteria}
  \|\mathbf{Pr}^{(t)}\|_F \leq \epsilon_{pr}^{(t)} \text{ and }     \|\mathbf{Dr}^{(t)}\|_F \leq \epsilon_{dr}^{(t)}. 
\end{eqnarray}

The authors of \cite{boyd-2010-distr-optim} suggest the following rules
\begin{eqnarray*}
  \epsilon_{pr}^{(t)} & = & \sqrt{nCK}~\epsilon_{abs} + \epsilon_{rel}\max\Bigg[\sqrt{\sum_{\ell=1}^n\|\mathbf{R}^{(t)}_\ell\|^2_F}, \sqrt{n\rho^{(t)}}\|\mathbf{Z}^{(t)}\|_F\Bigg]\\
  \epsilon_{dr}^{(t)} & = & \sqrt{nCK}~\epsilon_{abs} + \epsilon_{rel}\|\mathbf{Y}^{(t)}\|_F
\end{eqnarray*}
with \(\epsilon^{rel}=10^{-3}\) or \(10^{-4}\).
\subsubsection{\(\rho\) update strategy}
\label{sec:orgf1ef375}
It is possible to use different values of \(\rho^{(t)}\) for each iteration, in order to improve the convergence of the algorithm. \cite{boyd-2010-distr-optim} propose the following update strategy :

$$
\rho^{(t+1)} := \left\{\begin{matrix}
\tau^{incr}\rho^{(t)} & \text{if} \left \| \mathbf{Pr}^(t) \right \|_F > \mu \left \| \mathbf{Dr}^(t) \right \|_F\\ 
\rho^{(t)}  / \tau^{decr} & \text{if} \left \| \mathbf{Dr}^(t) \right \|_F > \mu \left \| \mathbf{Pr}^(t) \right \|_F\\  
\rho^{(t)} & \text{otherwise} 
\end{matrix}\right.
$$

\section{Semi-supervized approach}
\label{sec:org1cbe10a}
We can write the completed log-likelihood from eq (\ref{eq:ll}), using \(\gamma\) as the responsibility terms

\begin{eqnarray}
  \label{eq:cll}
  \begin{array}{rl}
    \displaystyle{\min_{\mathbf{R}}}
   &\displaystyle{\sum_{\ell=1}^n\sum_{c=1}^C -\gamma_{\ell c}\ln\big(\boldsymbol{\beta}_c^\top\mathbf{R}\boldsymbol{\psi}_\ell\big)}\\
   \text{Constraint to} & \mathbf{R}^\top\mathbf{1}_C = \mathbf{1}_K\\
  & \mathbf{R} \succcurlyeq 0,
  \end{array}
  \end{eqnarray}

An EM algorithm can be stated as follow, where \((q)\) denotes the current iteration:
\begin{itemize}
\item \textbf{E-step}: estimate \(\gamma_{\ell c}\) using posterior probability (\ref{eq:map}), which can be written as
\begin{eqnarray}
\boldsymbol{\gamma}_\ell^{(q+1)} &=& \mathbf{R}^{(q)}\boldsymbol{\psi}_\ell \text{ with } \boldsymbol{\gamma}_\ell = \big[\gamma_{\ell 1}, \ldots, \gamma_{\ell C}\big]^\top.\label{eq:ss:e} 
\end{eqnarray}
\item \textbf{M-step}: estimate \(\mathbf{R}\) with eq. (\ref{eq:cll}) and \(\boldsymbol{\gamma}_\ell^{(q+1)}\).
\end{itemize}

The \textbf{M-step} is similar to the problem described in section \ref{sec:consensus}. In particular, eq. (\ref{eq:consensus:fun:ell}) can be written in the following form, accounting for the responsibilities (where the index \(q\) is dropped for simplicity):

\begin{eqnarray}\label{eq:consensus:ss:fun:ell}
  F_\ell(\mathbf{R}_\ell) & = & -\sum_{i=1}^C\gamma_{\ell i}\ln(\mathbf{r}_{\ell i}^\top\boldsymbol{\psi}_\ell)_{+} + \sum_{i=1}^C\langle\mathbf{y}_{\ell i}^{(t)}, \mathbf{r}_{\ell i} - \mathbf{z}_i^{(t)}\rangle + \frac{\rho^{(t)}}{2}\|\mathbf{r}_{\ell i} - \mathbf{z}_i^{(t)}\|^2.
\end{eqnarray}

Using the same calculus than for the general consensus approach, the following results holds

\begin{eqnarray}
  \label{eq:consensus:ss:rl}
  \mathbf{r}_{\ell i}^{(t+1)} = \mathbf{z}_{i}^{(t)} + \frac{1}{\rho^{(t)}}\Bigg(\gamma_{\ell i}\frac{\boldsymbol{\psi}_\ell}{\nu_{\ell i}^{(t)}} - \mathbf{y}_{\ell i}^{(t)}\Bigg),\ \forall(\ell,\ i)\in\{1,\ldots,n\}\times\{1,\ldots,C\}
\end{eqnarray}
with
\begin{eqnarray*}
  \nu_{\ell i}^{(t)} = \frac{1}{2}\boldsymbol{\psi}_{\ell i}^\top\bigg(\mathbf{z}_i^{(t)}-\frac{\mathbf{y}_{\ell i}^{(t)}}{\rho^{(t)}}\bigg) + \sqrt{\Bigg(\frac{1}{4}\boldsymbol{\psi}_{\ell}^\top\bigg(\mathbf{z}_i^{(t)}-\frac{\mathbf{y}_{\ell i}^{(t)}}{\rho^{(t)}}\bigg)\Bigg)^2 +\gamma_{\ell i}\frac{\|\boldsymbol{\psi}_\ell\|^2}{\rho^{(t)}}}
\end{eqnarray*}

\textbf{TODO}
\begin{itemize}
\item Express visually and formally what are \(\mathbf{y}\) and \(\mathbf{z}\).
\item Express the problem when only a part on the responsibilities are updated
\end{itemize}
\section{Reference}
\label{sec:org5f53dd6}

\bibliographystyle{alpha}
\bibliography{references}
\end{document}
